<?php
/*
Plugin Name:  BusinessList
Plugin URI:   https://www.wpBusiness.com
Description:  A short little description of the plugin. It will be displayed on the Plugins page in WordPress admin area. 
Version:      1.0
Author:       Aurore Bouriot
*/

// Add menu
function display_hello_world_page() {
    echo 'BusinessList!';
}
function displayData(){
    global $wpdb;
    $tablename = $wpdb->prefix."annuaire"; 
    ?>
   
    <table width='95%' border='1' style='border-collapse: collapse;'>
    <tr>
    <th>S.no</th>
    <th>name</th>
    <th>location</th>
    <th>first name</th>
    <th>last name</th>
    <th>email</th>
    </tr>
    <?php
    // Select records
    $entriesList = $wpdb->get_results("SELECT * FROM ".$tablename." order by id desc");
    if(count($entriesList) > 0){
        $count = 1;
        foreach($entriesList as $entry){
        $id = $entry->id;
        $name = $entry->nom_entreprise;
        $location = $entry->localisation_entreprise;
        $firstName = $entry->prenom_contact;
        $lastName = $entry->nom_contact;
        $email = $entry->mail_contact;

        echo "<tr>
        <td>".$count."</td>
        <td>".$name."</td>
        <td>".$location."</td>
        <td>".$firstName."</td>
        <td>".$lastName."</td>
        <td>".$email."</td>
        </tr>
        ";
        $count++;
    }
    }else{
    var_dump($wpdb);
    echo "<tr><td colspan='5'>No record found</td></tr>";
    }
    ?>
    </table> <?php
}
function hello_world_admin_menu() {
add_menu_page(
        'BusinessList',// page title
        'BusinessList',// menu title
        'manage_options',// capability
        'business-list',// menu slug
        'display_hello_world_page' // callback function
    );
add_submenu_page( 'business-list', 'All Data', 'All Data', 'manage_options', 'all_entries', 'displayData' );
}
add_action('admin_menu', 'hello_world_admin_menu');

add_shortcode( 'business_list', 'displayData' );

?>
